import { Component } from "react";
import { Button, Modal } from "./UI";
import styles from "./App.module.scss";
import {
  FIRST_COLOR,
  MODAL_FIRST_CONTENT,
  MODAL_SECOND_CONTENT,
  SECOND_COLOR,
} from "./utils/const";

class App extends Component {
  state = {
    modalAction: false,
    modalContent: {
      modalHeader: "",
      modalText: "",
    },
  };

  handleOpenModal = () => {
    this.setState({ modalAction: true });
  };
  handleCloseModal = () => {
    this.setState({ modalAction: false });
  };

  handleChangeContentModalFirst = () => {
    this.setState({
      modalContent: {
        modalHeader: MODAL_FIRST_CONTENT.HEADER,
        modalText: MODAL_FIRST_CONTENT.TEXT,
      },
    });
  };
  handleChangeContentModalSecond = () => {
    this.setState({
      modalContent: {
        modalHeader: MODAL_SECOND_CONTENT.HEADER,
        modalText: MODAL_SECOND_CONTENT.TEXT,
      },
    });
  };

  handleFirstButton = () => {
    this.handleOpenModal();
    this.handleChangeContentModalFirst();
  };
  handleSecondButton = () => {
    this.handleOpenModal();
    this.handleChangeContentModalSecond();
  };

  render() {
    const { modalContent, modalAction } = this.state;
    const { modalHeader, modalText } = modalContent;

    return (
      <div className={styles.app}>
        <Button backgroundColor={FIRST_COLOR} onClick={this.handleFirstButton}>
          Open first modal
        </Button>
        <Button
          backgroundColor={SECOND_COLOR}
          onClick={this.handleSecondButton}
        >
          Open second modal
        </Button>
        {modalAction && (
          <Modal
            onClose={this.handleCloseModal}
            header={modalHeader}
            text={modalText}
            closeButton={true}
          >
            <Button backgroundColor={FIRST_COLOR}>Ok</Button>
            <Button
              backgroundColor={SECOND_COLOR}
              onClick={this.handleCloseModal}
            >
              Cancel
            </Button>
          </Modal>
        )}
      </div>
    );
  }
}

export default App;
