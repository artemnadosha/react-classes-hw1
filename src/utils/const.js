export const FIRST_COLOR = "#03c139";
export const SECOND_COLOR = "#dc3535";

export const MODAL_FIRST_CONTENT = {
  HEADER: "Do you want to delete this file?",
  TEXT: "Once you delete this file, it won't be possible to undo this action. Are you sure you want yo delete it?",
};

export const MODAL_SECOND_CONTENT = {
  HEADER: "Lorem ipsum dolor sit amet.",
  TEXT: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio harum molestiae perferendis quos saepe tempore.",
};
