import { Component } from "react";
import styles from "./Modal.module.scss";
class Modal extends Component {
  render() {
    const { onClose, header, text, children, closeButton } = this.props;

    return (
      <div className={styles.wrapper}>
        <div className={styles.overlay} onClick={onClose}></div>
        <div className={styles.modalContent}>
          <div className={styles.wrapperHeader}>
            <h1>{header}</h1>
            {closeButton && (
              <div className={styles.closeIcon} onClick={onClose}></div>
            )}
          </div>
          <p>{text}</p>
          <div className={styles.wrapperActions}>{children}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
