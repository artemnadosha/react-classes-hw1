import { Component } from "react";
import styles from "./Button.module.scss";
class Button extends Component {
  render() {
    const { children, backgroundColor, onClick } = this.props;
    return (
      <button
        className={styles.button}
        onClick={onClick}
        style={{ backgroundColor: backgroundColor }}
      >
        <p>{children}</p>
      </button>
    );
  }
}

export default Button;
